declare module 'rpi-gpio' {
  export default class {
    static DIR_HIGH: string
    static DIR_OUT: string
    static MODE_BCM: string

    static setMode (mode: string): void
  }
  export class promise {
    static setup (channel: number, direction: string): Promise<void>
    static write (channel: number, value: boolean): Promise<void>
  }
}